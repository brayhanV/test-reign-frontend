FROM node:alpine as build-step

RUN mkdir /app
WORKDIR /app
COPY package.json /app
RUN npm config set registry http://registry.npmjs.org
RUN npm install
COPY . /app
RUN npm run build

FROM nginx:alpine
COPY --from=build-step /app/build/ /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]