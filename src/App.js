import "./App.css";
import { request } from "./utils/Request";
import React, { useEffect, useState } from "react";
import { Header } from "./Componets/Header";
import { ArticleItem } from "./Componets/ArticleItem";

function App() {
	const [articles, setArticles] = useState([]);

	const getArticles = async () => {
		try{
			const {data} = await request.get("/articles");

			setArticles(data);
		}catch(error){
			console.error(error);
			alert(error);
		}
	};

	useEffect(() => {
		getArticles();
	}, []);

	return (
		<div className="container">
			<Header />
			{articles.map(elem => (
				<ArticleItem key={elem._id} item={elem} refreshCallback={getArticles} />
			))}
		</div>
	);
}

export default App;
