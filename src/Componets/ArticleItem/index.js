/* eslint-disable react/prop-types */
import moment from "moment";
import React from "react";
import "./styles.css";
import { ReactComponent as DeleteIcon } from "../../assets/images/delete-icon.svg";
import { request } from "../../utils/Request";
import PropTypes from "prop-types";

export const ArticleItem = ({item, refreshCallback}) => {
	const deleteNew = async (e, id) => {
		e.stopPropagation();
		try{
			await request.delete(`/articles/${id}`);

			refreshCallback();
		}catch(error){
			alert(error);
		}
	};

	const openUrl = (storyUrl, url)  => {
		const win = window.open(storyUrl ? storyUrl : url, "_blank");
		win.focus();
	};

	return (
		<div className="rowItem" onClick={() => openUrl(item.storyUrl, item.url)}>
			<div className="col">
				<h3 className="title">{item.storyTitle ? item.storyTitle : item.title}</h3>            
				<p className="author">{item.author}</p>
			</div>

			<p className="left">{moment(item.createdAt).fromNow()}</p>
			<button className="button" onClick={e => deleteNew(e, item.objectID)}><DeleteIcon className="icon" /></button>
		</div>
	);
};

ArticleItem.protoTypes = {
	item: PropTypes.object,
	refreshCallback: PropTypes.func
};