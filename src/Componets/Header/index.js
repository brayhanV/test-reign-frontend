import React from "react";
import "./styles.css";

export const Header = () => (
	<div className="header">
		<h1>NH Feed</h1>
		<h3>We {"<3"} hacker news</h3>
	</div>
);