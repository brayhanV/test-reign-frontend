/* eslint-disable no-undef */
import axios from "axios";

export const request = axios.create({baseURL: process.env.REACT_APP_API_URL ? `http://${process.env.REACT_APP_API_URL}:3003` : "http://localhost:3003"});